
package tests;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

import core.BaseTest;

public class CepTest extends BaseTest{

	@Test
	public void validaCEPValido() {
		given()
		.when() 
		    .get("/91060900/json/")
		.then() 
		    .statusCode(200)
		    .body("cep", is("91060-900"))
		    .body("logradouro", is("Avenida Assis Brasil 3940"))
		    .body("complemento", is(""))
		    .body("bairro", is("S�o Sebasti�o"))
		    .body("localidade", is("Porto Alegre"))
		    .body("uf", is("RS"))
		    .body("ibge", is("4314902"))
		    .body("gia", is(""))
		    .body("ddd", is("51"))
		    .body("siafi", is("8801"));
	}
	
	@Test
	public void validaCEPInexistente() {
		given()
		.when() 
		    .get("/99999999/json/")
		.then() 
		    .statusCode(200)
		    .body("erro", is(true));
	}
	
	@Test
	public void validaCEPInvalido() {
		given()
		.when() 
		    .get("/8-)(2584/json/")
		.then() 
		    .statusCode(400)
		    .body("html.body.h1", equalTo("Erro 400"))
			.body("html.body.h2", equalTo("Ops!"))
			.body("html.body.h3", equalTo("Verifique a sua URL (Bad Request)"));

	}
	
	@Test
	public void validaRetornoEndereco() {
		given()
		.when() 
		    .get("/RS/Gravatai/Barroso/json/")
		.then() 
		    .statusCode(200)
		    .body("cep[0]", is("94085-170"))
		    .body("logradouro[0]", is("Rua Ari Barroso"))
		    .body("complemento[0]", is(""))
		    .body("bairro[0]", is("Morada do Vale I"))
		    .body("localidade[0]", is("Gravata�"))
		    .body("uf[0]", is("RS"))
		    .body("ibge[0]", is("4309209"))
		    .body("gia[0]", is(""))
		    .body("ddd[0]", is("51"))
		    .body("siafi[0]", is("8683"))
		    .body("cep[1]", is("94175-000"))
		    .body("logradouro[1]", is("Rua Almirante Barroso"))
		    .body("complemento[1]", is(""))
		    .body("bairro[1]", is("Recanto Corcunda"))
		    .body("localidade[1]", is("Gravata�"))
		    .body("uf[1]", is("RS"))
		    .body("ibge[1]", is("4309209"))
		    .body("gia[1]", is(""))
		    .body("ddd[1]", is("51"))
		    .body("siafi[1]", is("8683"));
	}
}
