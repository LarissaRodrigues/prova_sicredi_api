package core;

import static org.hamcrest.Matchers.lessThan;

import org.junit.BeforeClass;

import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;

public class BaseTest {

	@BeforeClass
	public static void setUp() {
		RestAssured.baseURI = "https://viacep.com.br/ws";
		ResponseSpecBuilder resBuilder = new ResponseSpecBuilder();
		resBuilder.expectResponseTime(lessThan(5000L));
		RestAssured.responseSpecification = resBuilder.build();
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
	}
}