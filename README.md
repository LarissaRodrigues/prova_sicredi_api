# Projeto Desafio Sicred - API

### 🛠️ Ferramentas

 - Java 8
 - Maven 3.8.1
 - Junit 4.12
 - Rest-Assured 4.0.0 
 
### Configuração do projeto
- Realizar o import do projeto na IDE desejada;
- Na raiz do projeto mvn clean, mvn install;

### Execução do projeto:
- Acessar ${basedir}/src/test/java/tests/CepTest.java executar o teste pela a IDE;